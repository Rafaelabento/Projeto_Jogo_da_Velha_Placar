package com.itau.model;

public class Placar {
    private String[][] casas;
    private int[] pontuacao = new int[2];
    private boolean encerrado;

    public String[][] getCasas() {
        return casas;
    }

    public void setCasas(String[][] casas) {
        this.casas = casas;
    }

    public int[] getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(int[] pontuacao) {
        this.pontuacao = pontuacao;
    }

    public boolean isEncerrado() {
        return encerrado;
    }

    public void setEncerrado(boolean encerrado) {
        this.encerrado = encerrado;
    }
}
