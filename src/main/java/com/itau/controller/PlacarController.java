package com.itau.controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.itau.model.Jogador;
import com.itau.model.Placar;
import com.itau.mq.Evento;
import com.itau.requester.ApiRequester;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class PlacarController {
    Placar placar = new Placar();

    @RequestMapping(path ="/placar", method = RequestMethod.GET)
    @CrossOrigin
    public @ResponseBody ResponseEntity<?> getPlacar(){
        try {
            Evento.send("g4.ms.Placar.PlacarController.getPlacar");
            String response = ApiRequester.get("http://localhost:8082/jogador/todos");
            List<Jogador> jogadores = new Gson().fromJson(response, new TypeToken<ArrayList<Jogador>>(){}.getType());
            int[] pontuacao = new int[jogadores.size()];
            for(int i = 0; i< jogadores.size(); i++){
                pontuacao[i] = jogadores.get(i).pontos;
            }
            placar.setPontuacao(pontuacao);
            return ResponseEntity.ok(placar);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getStackTrace());
        }
    }
}
